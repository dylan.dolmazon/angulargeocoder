import { Component, OnInit } from '@angular/core';
import {Todo} from '../model/todo';
import {Router} from '@angular/router';
import {TodoServicesService} from '../services/todo-services.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  public todos: Todo[] = [];
  public date= "";
  public name= "";
  public content= "";

  constructor(private router: Router , private todoService: TodoServicesService ) { }

  ngOnInit() {
    this.loadAllTodoList()
  }

  loadAllTodoList() {
    this.todos = this.todoService.getAllTodos();
  }

  onClickAddTodo(content: string, date: string, name: string) {
    this.todoService.addTodo(date,content,name);
    this.date = "";
    this.content= "";
    this.name= "";
    this.loadAllTodoList();
  }

  onClickTodoDelete(content: string) {
    this.todoService.deleteTodoDetail(content);
    this.loadAllTodoList();
  }

}
