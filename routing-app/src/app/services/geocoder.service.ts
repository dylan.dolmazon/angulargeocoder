import { Injectable } from '@angular/core';
import {createGeocoderAzure, GeocoderAzur, geocode, GeocoderData} from '@maasify/geocoder';
import {Geocoder} from '../../app/config/geocoder'

@Injectable({
  providedIn: 'root'
})
export class GeocoderService {

  public geocoderAsure: GeocoderAzur = createGeocoderAzure({apiKey: Geocoder.AZURE_KEY});

  constructor() {

  }
  GetAddress = (value: string): Promise<GeocoderData | null> => {
    return geocode([this.geocoderAsure], value).then((result) => {
      return (result?.length) ? result[0] : null;
    });
  }
}
