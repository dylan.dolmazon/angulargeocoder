import { Injectable } from '@angular/core';
import {Todo} from '../model/todo';

@Injectable({
  providedIn: 'root'
})
export class TodoServicesService {

  private todos: Todo[] = [];

  constructor() {
    this.addTodo("24/10/2022","premier todo","Dylan");
  }

  public getAllTodos(){
    return this.todos;
  }

  public addTodo(date: string, content: string, name: string){
    this.todos.push(new Todo(date, content,name));
  }

  public deleteTodoDetail(content: string){
    this.todos.forEach(todo => {
      if(todo.getContent() === content){
        this.todos.splice(this.todos.indexOf(todo),1)
      }
    })
  }

}
