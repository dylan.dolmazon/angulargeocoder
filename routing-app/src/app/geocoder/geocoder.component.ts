import {Component, Inject, OnInit} from '@angular/core';
import {GeocoderService} from '../services/geocoder.service'

@Component({
  selector: 'app-geocoder',
  templateUrl: './geocoder.component.html',
  styleUrls: ['./geocoder.component.css']
})

export class GeocoderComponent implements OnInit {

  value= "";
  geocoderData: any;

  constructor(private geocoderService: GeocoderService) {
    this.geocoderData = this.geocoderService.GetAddress(this.value);
  }

  ngOnInit(): void {

  }

  inputChanged(){
    this.geocoderData = this.geocoderService.GetAddress(this.value);
    console.log(this.geocoderData);
  }

}
