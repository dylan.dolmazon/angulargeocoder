export class Todo {


  private date = "";
  private  content = "";
  private name = "";

  constructor(date: string, content: string, name: string) {
    this.date = date;
    this.content = content;
    this.name = name;
  }

  public getName(): string {
    return this.name;
  }

  public setName(value: string) {
    this.name = value;
  }

  public getDate(){
    return this.date;
  }

  public getContent(){
    return this.content
  }

  public setDate(date: string){
    this.date = date;
  }

  public setContent(content: string){
    this.content = content;
  }

}
