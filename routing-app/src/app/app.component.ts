import {Component, OnInit} from '@angular/core';
import { TodoServicesService } from './services/todo-services.service';
import {Router} from '@angular/router';
import {Todo} from './model/todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'routing-app';

  ngOnInit() {
  }

}
