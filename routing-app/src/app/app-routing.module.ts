import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {GeocoderComponent} from './geocoder/geocoder.component';
import {TodoListComponent} from './todo-list/todo-list.component';

const routes: Routes = [
  { path: 'geocoder', component: GeocoderComponent },
  { path: 'todoList', component: TodoListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
