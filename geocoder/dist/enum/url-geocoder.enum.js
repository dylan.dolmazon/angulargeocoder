"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UrlGeocoder = void 0;
var UrlGeocoder;
(function (UrlGeocoder) {
    UrlGeocoder["AZURE_SEARCH"] = "https://atlas.microsoft.com/search/fuzzy/json?&api-version=1.0&subscription-key={token}&language=fr-FR&query={query}&typeahead=true&limit=10";
    UrlGeocoder["NAVITIA_SEARCH"] = "https://navitia.maasify.io/v1/coverage/{coverage}/places?q={query}&type%5B%5D=stop_area&count=5&";
})(UrlGeocoder = exports.UrlGeocoder || (exports.UrlGeocoder = {}));
