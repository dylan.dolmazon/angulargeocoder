"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Provider = void 0;
var Provider;
(function (Provider) {
    Provider["AZURE"] = "AZURE";
    Provider["NAVITIA"] = "NAVITIA";
})(Provider = exports.Provider || (exports.Provider = {}));
