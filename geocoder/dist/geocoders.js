"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.geocode = exports.createGeocoderAzure = exports.createGeocoderNavitia = void 0;
const url_geocoder_enum_1 = require("./enum/url-geocoder.enum");
const axios_1 = require("axios");
/**
 *
 * @param geocoderParam
 */
const createGeocoderNavitia = (geocoderParam) => {
    const geocoder = { ...geocoderParam };
    if (!geocoder.apiKey) {
        throw new Error("apiKey is required !");
    }
    if (!geocoder.coverage) {
        throw new Error("coverage is required !");
    }
    geocoder.provider = "NAVITIA";
    geocoder.type = ['stop_area'];
    geocoder.urlSearch = url_geocoder_enum_1.UrlGeocoder.NAVITIA_SEARCH.replace('{coverage}', geocoder.coverage);
    return geocoder;
};
exports.createGeocoderNavitia = createGeocoderNavitia;
/**
 *
 * @param geocoderParam
 */
const createGeocoderAzure = (geocoderParam) => {
    var _a, _b;
    const geocoder = { ...geocoderParam };
    if (!geocoder.apiKey) {
        throw new Error("apiKey is required !");
    }
    geocoder.provider = "AZURE";
    geocoder.urlSearch = url_geocoder_enum_1.UrlGeocoder.AZURE_SEARCH.replace('{token}', `${geocoder.apiKey}`);
    if (geocoderParam === null || geocoderParam === void 0 ? void 0 : geocoderParam.countrySet) {
        geocoder.urlSearch += `&countrySet=${geocoderParam.countrySet}`;
    }
    if ((geocoderParam === null || geocoderParam === void 0 ? void 0 : geocoderParam.position) && ((_a = geocoderParam === null || geocoderParam === void 0 ? void 0 : geocoderParam.position) === null || _a === void 0 ? void 0 : _a.lat) && ((_b = geocoderParam === null || geocoderParam === void 0 ? void 0 : geocoderParam.position) === null || _b === void 0 ? void 0 : _b.lon)) {
        geocoder.urlSearch += `&lat=${geocoderParam.position.lat}&lon=${geocoderParam.position.lon}`;
    }
    return geocoder;
};
exports.createGeocoderAzure = createGeocoderAzure;
/**
 *
 * @param geocoder
 * @param query
 */
const geocode = (geocoder, query) => {
    const promises = [];
    if (Array.isArray(geocoder)) {
        geocoder.forEach((geocoder) => {
            let authentication = undefined;
            const url = geocoder.urlSearch;
            const validUrl = composeUrl(url, query);
            if (geocoder.provider === "NAVITIA") {
                authentication = composeAuth(geocoder.apiKey);
            }
            promises.push(getData(validUrl, authentication, geocoder.provider));
        });
    }
    else {
        throw new Error('error Array Geocoder is required !');
    }
    return Promise.all(promises).then((values) => {
        const processingValue = values.reduce((a, b) => { return a.concat(b); });
        return processingValue.slice(0, 10);
    });
};
exports.geocode = geocode;
const composeAuth = (apiKey) => {
    return {
        username: apiKey,
        password: ''
    };
};
const composeUrl = (url, query) => {
    return url.replace('{query}', query);
};
const getData = (url, authentication, provider) => {
    if (!!url) {
        const config = { timeout: 1000, auth: authentication ? authentication : undefined };
        return axios_1.default.get(url, config).then((result) => {
            let processingData = [];
            switch (provider) {
                case 'NAVITIA':
                    processingData = processingDataNavitia(result.data.places);
                    break;
                case 'AZURE':
                    processingData = processingDataAzure(result.data.results);
                    break;
            }
            return processingData;
        }).catch((error) => {
            console.error(error.message);
            return [];
        });
    }
    else {
        throw new Error('error URL is required !');
    }
};
const processingDataAzure = (data) => {
    const processingData = [];
    data.forEach((result) => {
        var _a;
        let label = '';
        let poi = undefined;
        if (result.type === 'POI') {
            poi = {
                name: (result === null || result === void 0 ? void 0 : result.poi.name) ? result === null || result === void 0 ? void 0 : result.poi.name : '',
                classificationsCode: (_a = result === null || result === void 0 ? void 0 : result.poi) === null || _a === void 0 ? void 0 : _a.classifications[0].code
            };
        }
        if ((result === null || result === void 0 ? void 0 : result.entityType) === 'Municipality') {
            label += (result === null || result === void 0 ? void 0 : result.address.freeformAddress) ? `${result === null || result === void 0 ? void 0 : result.address.freeformAddress} ` : '';
            label += (result === null || result === void 0 ? void 0 : result.address.countrySubdivision) ? `${result === null || result === void 0 ? void 0 : result.address.countrySubdivision}` : '';
        }
        else {
            label += (result === null || result === void 0 ? void 0 : result.address.streetNumber) ? `${result === null || result === void 0 ? void 0 : result.address.streetNumber}` : '';
            label += (result === null || result === void 0 ? void 0 : result.address.streetName) ? ` ${result === null || result === void 0 ? void 0 : result.address.streetName}, ` : '';
            label += (result === null || result === void 0 ? void 0 : result.address.postalCode) ? `${result === null || result === void 0 ? void 0 : result.address.postalCode}` : '';
            label += (result === null || result === void 0 ? void 0 : result.address.municipality) ? `, ${result === null || result === void 0 ? void 0 : result.address.municipality}` : '';
            label += (result === null || result === void 0 ? void 0 : result.address.country) ? `, ${result === null || result === void 0 ? void 0 : result.address.country}` : '';
        }
        const place = {
            label: label,
            type: result === null || result === void 0 ? void 0 : result.type,
            position: result === null || result === void 0 ? void 0 : result.position,
            poi: poi ? poi : undefined,
            rawData: result,
        };
        processingData.push(place);
    });
    return processingData;
};
const processingDataNavitia = (data) => {
    const processingData = [];
    data.forEach((result) => {
        var _a;
        const place = {
            label: result === null || result === void 0 ? void 0 : result.name,
            type: result === null || result === void 0 ? void 0 : result.embedded_type,
            position: (_a = result === null || result === void 0 ? void 0 : result.stop_area) === null || _a === void 0 ? void 0 : _a.coord,
            poi: undefined,
            rawData: result
        };
        processingData.push(place);
    });
    return processingData;
};
