import { Geocoder, GeocoderAzur, GeocoderData, GeocoderNavitia } from "./interfaces/geocoder.interface";
/**
 *
 * @param geocoderParam
 */
export declare const createGeocoderNavitia: (geocoderParam: GeocoderNavitia) => GeocoderNavitia;
/**
 *
 * @param geocoderParam
 */
export declare const createGeocoderAzure: (geocoderParam: GeocoderAzur) => GeocoderAzur;
/**
 *
 * @param geocoder
 * @param query
 */
export declare const geocode: (geocoder: Geocoder[], query: string) => Promise<GeocoderData[]>;
