export interface Geocoder {
    apiKey: string,
    urlSearch?: string,
    provider?: string
}

export interface GeocoderNavitia extends Geocoder {
    coverage: string,
    type?: string[]
}

export interface GeocoderAzur extends Geocoder {
    countrySet?: string,
    position?: {
        lat: number,
        lon: number
    }
}

export interface GeocoderData {
    label: string,
    type: string,
    position: {
        lat: number,
        lon: number
    },
    poi?: Poi,
    rawData?: any
}

export interface Poi {
    name: string;
    classificationsCode: string;
}