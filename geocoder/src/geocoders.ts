import {Geocoder, GeocoderAzur, GeocoderData, GeocoderNavitia, Poi} from "./interfaces/geocoder.interface";
import {UrlGeocoder} from "./enum/url-geocoder.enum";
import axios, {AxiosBasicCredentials, AxiosRequestConfig} from "axios";

/**
 *
 * @param geocoderParam
 */
export const createGeocoderNavitia = (geocoderParam: GeocoderNavitia):GeocoderNavitia => {
    const geocoder = {...geocoderParam};
    if(!geocoder.apiKey){
        throw new Error("apiKey is required !");
    }
    if(!geocoder.coverage){
        throw new Error("coverage is required !");
    }
    geocoder.provider = "NAVITIA";
    geocoder.type = ['stop_area'];
    geocoder.urlSearch = UrlGeocoder.NAVITIA_SEARCH.replace('{coverage}',geocoder.coverage);
    return geocoder;
}

/**
 *
 * @param geocoderParam
 */
export const createGeocoderAzure = (geocoderParam: GeocoderAzur):GeocoderAzur => {
    const geocoder = {...geocoderParam};
    if(!geocoder.apiKey){
        throw new Error("apiKey is required !");
    }
    geocoder.provider = "AZURE";
    geocoder.urlSearch = UrlGeocoder.AZURE_SEARCH.replace('{token}',`${geocoder.apiKey}`);
    if(geocoderParam?.countrySet){
        geocoder.urlSearch += `&countrySet=${geocoderParam.countrySet}`;
    }
    if(geocoderParam?.position && geocoderParam?.position?.lat && geocoderParam?.position?.lon){
        geocoder.urlSearch += `&lat=${geocoderParam.position.lat}&lon=${geocoderParam.position.lon}`
    }
    return geocoder;
}

/**
 *
 * @param geocoder
 * @param query
 */
export const geocode = (geocoder:Geocoder[],query:string):Promise<GeocoderData[]>=>{
    const promises:Promise<GeocoderData[]>[]=[];
    if( Array.isArray(geocoder) ){
        geocoder.forEach((geocoder)=>{
            let authentication: AxiosBasicCredentials|undefined = undefined;
            const url =  geocoder.urlSearch;
            const validUrl = composeUrl(url as string,query);
            if(geocoder.provider === "NAVITIA"){authentication =  composeAuth(geocoder.apiKey)}
            promises.push(getData(validUrl,authentication,geocoder.provider));
        });
    }else {
        throw new Error('error Array Geocoder is required !');
    }
    return Promise.all(promises).then((values)=>{
        const processingValue = values.reduce((a,b)=>{return a.concat(b)})
        return processingValue.slice(0,10);
    })
}

const composeAuth = (apiKey:string):AxiosBasicCredentials => {
    return {
        username: apiKey,
        password: ''
    };
}

const composeUrl = (url:string,query:string):string => {
    return url.replace('{query}', query);
}

const getData =  (url : string,authentication:AxiosBasicCredentials|undefined,provider:string|undefined):Promise<GeocoderData[]> => {
    if (!!url) {
        const config:AxiosRequestConfig = {timeout:1000,auth: authentication ? authentication : undefined}
        return axios.get(url, config).then(
            (result) => {
                let processingData:GeocoderData[]=[];
                switch (provider){
                    case 'NAVITIA':
                        processingData = processingDataNavitia(result.data.places);
                        break;
                    case 'AZURE':
                        processingData = processingDataAzure(result.data.results);
                        break;
                }
                return processingData;
            }).catch(
            (error) => {
                console.error(error.message);
                return [];
            });
    } else{
        throw new Error('error URL is required !');
    }
}

const processingDataAzure=(data:any):GeocoderData[] => {
    const processingData:GeocoderData[]=[];
    data.forEach((result:any)=>{
        let label = '';
        let poi:Poi|undefined = undefined;
        if(result.type === 'POI'){
            poi = {
                name:result?.poi.name?result?.poi.name:'',
                classificationsCode:result?.poi?.classifications[0].code
            }
        }
        if(result?.entityType=== 'Municipality'){
            label += result?.address.freeformAddress?`${result?.address.freeformAddress} `:'';
            label += result?.address.countrySubdivision?`${result?.address.countrySubdivision}`:'';
        }else{
            label += result?.address.streetNumber ? `${result?.address.streetNumber}` : '';
            label += result?.address.streetName?` ${result?.address.streetName}, `:'';
            label += result?.address.postalCode?`${result?.address.postalCode}`:'';
            label += result?.address.municipality?`, ${result?.address.municipality}`:'';
            label += result?.address.country?`, ${result?.address.country}`:'';
        }
        const place:GeocoderData = {
            label :label,
            type:result?.type,
            position:result?.position,
            poi : poi?poi:undefined,
            rawData:result,
        }
        processingData.push(place);
    })
    return processingData;
}

const processingDataNavitia=(data:any):GeocoderData[] => {
    const processingData: GeocoderData[] = [];
    data.forEach((result: any) => {
        const place: GeocoderData = {
            label: result?.name,
            type: result?.embedded_type,
            position: result?.stop_area?.coord,
            poi:undefined,
            rawData:result
        }
        processingData.push(place);
    })
    return processingData;
}