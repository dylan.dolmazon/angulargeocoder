export enum UrlGeocoder {
    AZURE_SEARCH = 'https://atlas.microsoft.com/search/fuzzy/json?&api-version=1.0&subscription-key={token}&language=fr-FR&query={query}&typeahead=true&limit=10',
    NAVITIA_SEARCH = 'https://navitia.maasify.io/v1/coverage/{coverage}/places?q={query}&type%5B%5D=stop_area&count=5&'
}