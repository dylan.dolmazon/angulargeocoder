# GEOCODER
***
Géocodeur central pour la partie web, pour utiliser x geocodeurs dans un appel unique à cette lib

***
## Utilisation
### Créer un géocodeur
pour l'instant nous pouvons utiliser 2 géocoder dans la librairie Azure et Navitia

#### creer un géocoder Navitia
```ts
monGeocoderNavitia = geocoder.createGeocoderNavitia({
    apiKey:string, //(required) est le token pour navitia.
    coverage:string, //(required) correspond au coverage de navitia
});
```

#### creer un géocoder Azure
```ts
monGeocoderAzure = geocoder.createGeocoderAzure({
    apiKey:string, //(required) est le token pour azure.
	countrySet: string, //(optional) code pays FR pour la france
	position: { //(optional) latitude et longitude pour donner la prioritée aux alentours
		lat: number, 
		lon: number
	}
});
```

### lancer un geocodage
le géocodeur va ramener les 10 premiers résultats sans les réordonner 
> geocoder(required): Un tableau de géocoder vous pouvez passer un ou plusieur geocodeur
> 
> `Attention si vous utilisez Navitia et Azure ensemble il faut impérativement mettre Navitia en premier`
>
> query(required):string correspond à la recherche
```
geocoder.geocode(geocoder:geocoder[],query:string);
```